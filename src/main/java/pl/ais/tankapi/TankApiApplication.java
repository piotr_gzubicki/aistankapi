package pl.ais.tankapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Spring Boot Application class
 */
@SpringBootApplication
public class TankApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TankApiApplication.class, args);
    }

}
