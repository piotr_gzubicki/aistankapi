package pl.ais.tankapi.dto;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import pl.ais.tankapi.api.TankView;
import pl.ais.tankapi.api.TankView.TankId;

public class TankDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonView({ TankView.TankInfo.class, TankId.class })
    @JsonProperty("tank_id")
    private Long tankId;

    @JsonView(TankView.TankCreate.class)
    @JsonProperty("name")
    @NotNull(message = "Please provide tank name")
    private String name;

    @JsonView(TankView.TankCreate.class)
    @JsonProperty("weight")
    @NotNull(message = "Please provide tank weight")
    @Min(value = 1, message = "Tank weight should be greater than 0")
    private Integer weight;

    public String getName() {
        return name;
    }

    public Integer getWeight() {
        return weight;
    }

    public Long getTankId() {
        return tankId;
    }

    public void setTankId(Long tankId) {
        this.tankId = tankId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public static TankDto getInstance(String name, Integer weight) {
        TankDto dto = new TankDto();
        dto.setName(name);
        dto.setWeight(weight);
        return dto;
    }

    @Override
    public int hashCode() {
        if (tankId != null) {
            return tankId.hashCode();
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof TankDto) {
            TankDto dtoObj = (TankDto) obj;
            if (this.tankId != null && dtoObj.tankId != null) {
                return this.tankId.equals(dtoObj.tankId);
            }
        }
        return false;
    }
}
