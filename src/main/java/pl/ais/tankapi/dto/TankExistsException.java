package pl.ais.tankapi.dto;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class TankExistsException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public TankExistsException(String tankName) {
        super(String.format("Tank with name '%s' already exists", tankName));
    }

}
