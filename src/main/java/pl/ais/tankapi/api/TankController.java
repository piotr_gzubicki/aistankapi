package pl.ais.tankapi.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import pl.ais.tankapi.dto.TankDto;

/**
 * Tank api controller
 *
 */
@RestController
@RequestMapping("/tanks")
public class TankController {

    @Autowired
    private TankService tankService;

    /**
     * Lists all tanks
     * 
     * @return json list of all tanks ordered by name
     */
    @JsonView(TankView.TankInfo.class)
    @GetMapping
    public List<TankDto> getTankList() {
        return tankService.getTankList();
    }

    /**
     * Creates a new tank
     * 
     * @param tankDto - name and weight of tank to be created
     * @return json with tank_id
     */
    @JsonView(TankView.TankId.class)
    @PostMapping
    public TankDto createTank(@Validated @RequestBody @JsonView(TankView.TankCreate.class) TankDto tankDto) {
        return tankService.addTank(tankDto);
    }

    /**
     * Deletes a tank
     * 
     * @param tankId - id of a tank to be deleted
     */
    @DeleteMapping("/{tankId}")
    public void deleteTank(@PathVariable Long tankId) {
        tankService.deleteTank(tankId);
    }

}