package pl.ais.tankapi.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.hibernate.exception.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import pl.ais.tankapi.dto.TankDto;
import pl.ais.tankapi.dto.TankExistsException;
import pl.ais.tankapi.repo.Tank;
import pl.ais.tankapi.repo.TankRepository;

/**
 * Tank api db service
 *
 */
@Service
public class TankService {

    private final TankRepository tankRepo;
    private final ModelMapper modelMapper;

    @PersistenceContext
    private EntityManager em;

    public TankService(TankRepository tankRepo, ModelMapper modelMapper) {
        super();
        this.tankRepo = tankRepo;
        this.modelMapper = modelMapper;
    }

    /**
     * Lists all tanks
     * 
     * @return list of all tanks ordered by name
     */
    public List<TankDto> getTankList() {
        return tankRepo.findAllByOrderByNameAsc().stream().map(this::toDto).collect(Collectors.toList());
    }

    /**
     * Deletes a tank
     * 
     * @param tankId - id of a tank to be deleted
     */
    @Transactional
    public void deleteTank(Long tankId) {
        tankRepo.deleteById(tankId);
    }

    /**
     * Creates a new tank
     * 
     * @param tankDto - name and weight of tank to be created
     * @return tankDto with full tank data
     */
    @Transactional
    public TankDto addTank(TankDto dto) {
        Assert.isNull(dto.getTankId(), "tank id should be null");
        Tank tank = fromDto(dto);
        try {
            TankDto res = toDto(tankRepo.save(tank));
            em.flush();
            return res;
        } catch (PersistenceException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                ConstraintViolationException ce = (ConstraintViolationException) e.getCause();
                if (Tank.NAME_CONSTRAINT.equals(ce.getConstraintName())) {
                    throw new TankExistsException(dto.getName());
                }
            }
            throw e;
        }
    }

    public TankDto toDto(Tank tank) {
        return modelMapper.map(tank, TankDto.class);
    }

    public Tank fromDto(TankDto dto) {
        return modelMapper.map(dto, Tank.class);
    }

}
