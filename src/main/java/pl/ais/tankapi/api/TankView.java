package pl.ais.tankapi.api;

/**
 * List od all views in Tank api
 */
public class TankView {

    private TankView() {

    }

    /**
     * JSON parameter of view for creating a tank
     *
     */
    public static interface TankCreate {

    }

    /**
     * JSON view returning only id of a tank
     *
     */
    public static interface TankId {

    }

    /**
     * JSON view returning full tank info
     *
     */
    public static interface TankInfo extends TankCreate {

    }
}
