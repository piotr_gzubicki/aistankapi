package pl.ais.tankapi.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TankRepository extends CrudRepository<Tank, Long> {

    public List<Tank> findAllByOrderByNameAsc();

    public Boolean existsByName(String name);
}
