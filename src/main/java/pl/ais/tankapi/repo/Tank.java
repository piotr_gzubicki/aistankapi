package pl.ais.tankapi.repo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "name", name = Tank.NAME_CONSTRAINT))
public class Tank implements Serializable {

    public static final String NAME_CONSTRAINT = "tank_name_uniq";

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tank_tank_id_seq")
    @SequenceGenerator(name = "tank_tank_id_seq", sequenceName = "tank_tank_id_seq", allocationSize = 1)
    private Long tankId;

    private String name;

    private Integer weight;

    public Long getTankId() {
        return tankId;
    }

    public void setTankId(Long tankId) {
        this.tankId = tankId;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
