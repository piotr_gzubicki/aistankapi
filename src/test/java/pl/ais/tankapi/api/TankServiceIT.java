package pl.ais.tankapi.api;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import pl.ais.tankapi.dto.TankDto;
import pl.ais.tankapi.dto.TankExistsException;
import pl.ais.tankapi.repo.TankRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "it")
public class TankServiceIT {

    @Autowired
    private TankService tankService;

    @Autowired
    private TankRepository tankRepository;

    @Test
    public void shouldCreateTank() {
        TankDto res = tankService.addTank(TankDto.getInstance("T-34-85", 32));
        assertThat(res.getTankId()).isNotNull();
        assertThat(res.getName()).isNotNull();
        assertThat(res.getWeight()).isNotNull();
        assertThat(tankRepository.findById(res.getTankId()).isPresent()).isTrue();
    }

    @Test
    public void shouldDeleteTank() {
        TankDto res = tankService.addTank(TankDto.getInstance("T-34", 26));
        tankService.deleteTank(res.getTankId());
        assertThat(tankRepository.findById(res.getTankId()).isPresent()).isFalse();
    }

    @Test(expected = TankExistsException.class)
    public void shouldNotCreateTankWithTheSameName() {
        tankService.addTank(TankDto.getInstance("7TP", 10));
        tankService.addTank(TankDto.getInstance("7TP", 10));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotModifyTank() {
        TankDto res = tankService.addTank(TankDto.getInstance("Panzerkampfwagen V Panther", 45));
        res.setName("Panther");
        tankService.addTank(res);
    }

    @Test
    @Transactional
    public void shouldReturnProperOrdering() {
        TankDto i = tankService.addTank(TankDto.getInstance("IS-2", 46));
        TankDto sm = tankService.addTank(TankDto.getInstance("Sherman M4A3E8 (Easy Eight)", 33));
        TankDto p = tankService.addTank(TankDto.getInstance("Panzerkampfwagen VI Tiger Ausf. E", 57));
        TankDto t = tankService.addTank(TankDto.getInstance("T-34-85", 32));
        TankDto sf = tankService.addTank(TankDto.getInstance("Sherman Firefly", 35));
        TankDto tp7 = tankService.addTank(TankDto.getInstance("7TP", 10));
        TankDto c = tankService.addTank(TankDto.getInstance("Cruiser Mk VIII Challenger", 32));

        List<TankDto> list = tankService.getTankList();
        assertThat(list).containsExactly(tp7, c, i, p, sf, sm, t);
    }

    @After
    public void afterEach() {
        this.cleanUp();
    }

    @Before
    public void beforeEach() {
        this.cleanUp();
    }

    public void cleanUp() {
        tankRepository.deleteAll();
    }

}
