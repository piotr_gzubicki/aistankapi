package pl.ais.tankapi.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import pl.ais.tankapi.dto.TankDto;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TankController.class, secure = true)
public class TankControllerTests {

    private static final String TANKS_URL = "/tanks";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TankService tankService;

    @Test
    public void shouldReturnCorrectJsonTankList() throws Exception {
        TankDto tank1 = TankDto.getInstance("T-34-85", 32);
        tank1.setTankId(1L);
        TankDto tank2 = TankDto.getInstance("Panzerkampfwagen V Panther", 45);
        tank2.setTankId(2L);
        Mockito.when(tankService.getTankList()).thenReturn(Arrays.asList(tank1, tank2));
        MvcResult result = mockMvc.perform(get(TANKS_URL)).andDo(print()).andExpect(status().isOk()).andReturn();
        String expected = "[{\"tank_id\":1,\"name\":\"T-34-85\",\"weight\":32},{\"tank_id\":2,\"name\":\"Panzerkampfwagen V Panther\",\"weight\":45}]";
        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(expected);
    }

    @Test
    public void shouldReturnTankIdOnly() throws Exception {
        String tankParamJson = "{\"name\":\"T-34-85\",\"weight\":32}";
        TankDto tankResult = TankDto.getInstance("T-34-85", 32);
        tankResult.setTankId(1L);
        Mockito.when(tankService.addTank(Mockito.any())).thenReturn(tankResult);
        MvcResult result = mockMvc
                .perform(post(TANKS_URL).contentType(MediaType.APPLICATION_JSON_UTF8).content(tankParamJson))
                .andExpect(status().isOk()).andReturn();
        String expected = "{\"tank_id\":1}";
        assertThat(result.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(expected);
    }
    
    @Test
    public void shouldValidateWeight() throws Exception {
        String tankParamJson = "{\"name\":\"T-34-85\",\"weight\":-1}";
        mockMvc
                .perform(post(TANKS_URL).contentType(MediaType.APPLICATION_JSON_UTF8).content(tankParamJson))
                .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void shouldRequireWeight() throws Exception {
        String tankParamJson = "{\"name\":\"T-34-85\"}";
        mockMvc
                .perform(post(TANKS_URL).contentType(MediaType.APPLICATION_JSON_UTF8).content(tankParamJson))
                .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void shouldRequireName() throws Exception {
        String tankParamJson = "{\"weight\":1}";
        mockMvc
                .perform(post(TANKS_URL).contentType(MediaType.APPLICATION_JSON_UTF8).content(tankParamJson))
                .andExpect(status().is4xxClientError());
    }
}
